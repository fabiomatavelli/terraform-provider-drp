module gitlab.com/rackn/terraform-provider-drpv4

go 1.13

require (
	github.com/VictorLowther/jsonpatch2 v1.0.0
	github.com/go-test/deep v1.0.3
	github.com/hashicorp/terraform v0.13.2
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.0.1
	github.com/pborman/uuid v1.2.0
	gitlab.com/rackn/provision/v4 v4.8.0-alpha00.0.20210905035228-7afe04a243cf
)
